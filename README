-*- org -*-

#+TITLE Dave's virtual notebook for "Software Design for Flexibility"

* About

This repository is a place for me to collect example code, exercises,
and notes about the book [[https://mitpress.mit.edu/books/software-design-flexibility][Software Design for Flexibility]] by Chris
Hanson and Gerald Sussman.

The code in the book is written for MIT/GNU Scheme.  The code in this
repository is written for GNU Guile.  Example code has been adjusted
and shims have been added where necessary.

There is a directory for each chapter.  In each directory there is a
source file for each section.  The source files are annotated with
comments marking the subsection that example code was taken from and
the exercises that the solutions are for.

I made this repository to organize my own thoughts and work as I
slowly chew on this advanced tome, but I've made it public in case it
is useful to any fellow parenthesis nuts out there.

* License

All code in the book, my exercise solutions, and all other supporting
code I have implemented is licensed under GPL version 3 or later.

Notably, Gerald Sussman taught a remote-only class using a draft of
this book during the COVID-19 pandemic [[https://www.gnu.org/education/teaching-my-mit-classes-with-only-free-libre-software.en.html][using only free software]].

Follow Sussman's lead and share the software, friends.
