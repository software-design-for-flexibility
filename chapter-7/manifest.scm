(use-modules (guix git-download)
             (guix packages)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages guile-xyz))

(define guile-hoot-next
  (let ((commit "addbcd33158265a6c00e87aeb89d9c20f57bce1e")
        (revision "1"))
    (package
      (inherit guile-hoot)
      (version (git-version "0.4.1" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/spritely/guile-hoot.git")
                      (commit commit)))
                (file-name (git-file-name "guile-hoot" version))
                (sha256
                 (base32 "1zv8w9pawr3wajlcr862x21198x421fa47qbfkblnidiknfgs7sa"))))
      (arguments
       '(#:tests? #f)))))

(packages->manifest (list guile-next guile-hoot-next gnu-make))
